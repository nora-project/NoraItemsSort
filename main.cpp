#include <iostream>
#include "nlohmann/json.hpp"
#include <fstream>
//#include <bits/stdc++.h>
#include <string>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>
#include <iomanip>

#ifdef _WIN32
    #include <windows.h>
    #include <ShellAPI.h>
#else
    #include <cstdlib>
#endif  

using json = nlohmann::json;
using namespace std;

const string kPathSeparator =
#ifdef _WIN32
        "\\";
#else
        "/";
#endif

short exclude_last = 2;
short minimum_reserved = 5;
unsigned int pointer_size = 8;
unsigned int zukan_size = 4;
string entry_delimiter = "0000";
string entry_delimiter_custom = "0000";

vector<vector<string>> script_entries_original, script_entries_translation;

void findAndReplace(string &data, string toSearch, string replaceStr, bool all)
{
    // Get the first occurrence
    size_t pos = data.find(toSearch);
    // Repeat till end is reached
    while (pos != string::npos)
    {
        // Replace this occurrence of Sub String
        data.replace(pos, toSearch.size(), replaceStr);
        if (!all)
        {
            return;
        }
        // Get the next occurrence from the current position
        pos = data.find(toSearch, pos + replaceStr.size());
    }
}
#ifdef _WIN32
void executeCommand(string exec, string parameters)
{
    SHELLEXECUTEINFO ShExecInfo = {0};
    ShExecInfo.cbSize = sizeof(SHELLEXECUTEINFO);
    ShExecInfo.fMask = SEE_MASK_NOCLOSEPROCESS;
    ShExecInfo.hwnd = NULL;
    ShExecInfo.lpVerb = NULL;
    ShExecInfo.lpFile = exec.c_str();
    ShExecInfo.lpParameters = parameters.c_str();
    ShExecInfo.lpDirectory = NULL;
    ShExecInfo.nShow = SW_HIDE;
    ShExecInfo.hInstApp = NULL;
    ShellExecuteEx(&ShExecInfo);
    WaitForSingleObject(ShExecInfo.hProcess, INFINITE);
    CloseHandle(ShExecInfo.hProcess);
}

int dumpBinaryToHex(string rawFile, string tempPath)
{

    string commandDumpBinToHex("/C sfk.exe hexdump -pure -nofile -nolf ");
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

    executeCommand("cmd.exe", commandDumpBinToHex);

    ifstream hexItemsRead{tempPath};
    string stringItemsHexRead{std::istreambuf_iterator<char>(hexItemsRead), std::istreambuf_iterator<char>()};
    hexItemsRead.close();

    //If size 0(false), negated (true)
    if (!stringItemsHexRead.size())
    {
        return -1;
    }

    return 0;
}

int hexDumpToBinary(string hexdump_path, string temp_bin_path)
{

    string command_hexdump_to_binary("/C sfk.exe filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    executeCommand("cmd.exe", command_hexdump_to_binary);

    ifstream hexItemsRead{hexdump_path};
    string stringItemsHexRead{std::istreambuf_iterator<char>(hexItemsRead), std::istreambuf_iterator<char>()};
    hexItemsRead.close();

    //If size 0(false), negated (true)
    if (!stringItemsHexRead.size())
    {
        return -1;
    }

    return 0;
}
#else
void executeCommand(string exec,string parameters){
    string command=exec+" "+parameters;
    std::system(command.c_str());
}
int dumpBinaryToHex(string rawFile, string tempPath)
{

    string commandDumpBinToHex(" hexdump -pure -nofile -nolf ");
    commandDumpBinToHex.append(rawFile);
    commandDumpBinToHex.append(" > ");
    commandDumpBinToHex.append(tempPath);

    executeCommand("sfk", commandDumpBinToHex);

    ifstream hexItemsRead{tempPath};
    string stringItemsHexRead{std::istreambuf_iterator<char>(hexItemsRead), std::istreambuf_iterator<char>()};
    hexItemsRead.close();

    //If size 0(false), negated (true)
    if (!stringItemsHexRead.size())
    {
        return -1;
    }

    return 0;
}

int hexDumpToBinary(string hexdump_path, string temp_bin_path)
{

    string command_hexdump_to_binary(" filter ");
    command_hexdump_to_binary.append(hexdump_path);
    command_hexdump_to_binary.append(" +hextobin ");
    command_hexdump_to_binary.append(temp_bin_path);

    executeCommand("sfk", command_hexdump_to_binary);

    ifstream hexItemsRead{hexdump_path};
    string stringItemsHexRead{std::istreambuf_iterator<char>(hexItemsRead), std::istreambuf_iterator<char>()};
    hexItemsRead.close();

    //If size 0(false), negated (true)
    if (!stringItemsHexRead.size())
    {
        return -1;
    }

    return 0;
}
#endif

void writeFile(std::ostream &fileToWrite, string stringToWrite)
{
    fileToWrite << stringToWrite;
}

bool mycomp(string a, string b)
{
    //returns 1 if string a is alphabetically
    //less than string b
    //quite similar to strcmp operation
    return a < b;
}

vector<string> alphabaticallySort(vector<string> a)
{
    int n = a.size();
    //mycomp function is the defined function which
    //sorts the strings in alphabatical order
    sort(a.begin(), a.end(), mycomp);
    return a;
}

string getLEString(string stringHexIn, bool reverse, bool pointers, int byteSize)
{
    string little_endian_h = "";
    int sizestr;
    if (pointers)
    {
        sizestr = byteSize * 2;
    }
    else
    {
        sizestr = byteSize * 2;
    }
    for (unsigned int i = 0; i < (unsigned int)stringHexIn.length(); i += sizestr)
    {
        string thisCharacter = stringHexIn.substr(i, sizestr);

        if (thisCharacter == entry_delimiter && !pointers)
        {
            little_endian_h.append(entry_delimiter_custom);
        }
        else
        {
            vector<string> bytes;
            for (unsigned int j = 0; j < (unsigned int)thisCharacter.length(); j += 2)
            {
                bytes.push_back(thisCharacter.substr(j, 2));
            }

            if (reverse)
            {
                for (int j = bytes.size() - 1; j >= 0; j--)
                {
                    little_endian_h.append(bytes.at(j));
                }
            }
            else
            {
                for (unsigned int j = 0; j < (unsigned int)bytes.size(); j++)
                {
                    little_endian_h.append(bytes.at(j));
                }
            }
        }
    }
    //To Uppercase
    transform(little_endian_h.begin(), little_endian_h.end(),
              little_endian_h.begin(), ::toupper);

    return little_endian_h;
}

//function to print the array
void print(vector<string> names)
{
    printf("printing ........\n");
    for (int i = 0; i < names.size(); i++)
        cout << names[i] << endl;
    printf("\n");
}

unsigned int fromHex(const string &s) { return strtoul(s.c_str(), NULL, 16); }

string toHex(unsigned int num)
{
    stringstream stream;
    stream << setfill('0') << setw(zukan_size) << hex << num;
    string result(stream.str());

    transform(result.begin(), result.end(), result.begin(), ::toupper);

    return result;
}

void initGlobalVectors(json items_json)
{

    script_entries_original.clear();
    script_entries_translation.clear();
    /*
    script_entries_font_width.clear();
    script_entries_extra_space.clear();
    */
    for (unsigned int i = 0; i < items_json.size(); i++)
    {
        /*
        bool es=items_json[to_string(i+1)]["extra_space"].get<bool>();
        bool fw=items_json[to_string(i+1)]["full_width"].get<bool>();
        script_entries_extra_space.push_back(es);
        script_entries_font_width.push_back(fw);
        
        */
        vector<string> lines;
        for (unsigned int j = 0; j < items_json[to_string(i + 1)]["original"].size(); j++)
        {
            lines.push_back(items_json[to_string(i + 1)]["original"][to_string(j + 1)]["text"].get<std::string>());
        }
        script_entries_original.push_back(lines);
        lines.clear();
        for (unsigned int j = 0; j < items_json[to_string(i + 1)]["translation"].size(); j++)
        {
            lines.push_back(items_json[to_string(i + 1)]["translation"][to_string(j + 1)]["text"].get<std::string>());
        }
        script_entries_translation.push_back(lines);
    }
}

vector<string> remove_non_indexable(vector<vector<string>> entries, vector<unsigned int> exclude)
{
    vector<string> out;
    for (unsigned int i = 0; i < entries.size(); i++)
    {
        bool is_excluded = false;
        for (unsigned int j = 0; j < exclude.size(); j++)
        {
            if (i == exclude.at(j))
            {
                is_excluded = true;
                break;
            }
        }
        if (!is_excluded)
        {

            /* code */
            try
            {
                /* code */
                out.push_back(script_entries_translation.at(i).at(0));
            }
            catch (const std::exception &e)
            {
                std::cerr << e.what() << '\n';
            }
        }
    }

    return out;
}

void exec_ba_monster_files(string prepatched_binary_path, string og_ptrs_section_path,
                           string start_offset_section, string end_offset_section, vector<unsigned int> unsorted_index_ba_monster)
{

    //Read original pointers section
    ifstream hex_ba_monster_part_read{og_ptrs_section_path};
    string string_ba_monster_part_hex{std::istreambuf_iterator<char>(hex_ba_monster_part_read), std::istreambuf_iterator<char>()};
    hex_ba_monster_part_read.close();

    //Convert unsorted indexes to a vector of hex strings, little endian
    vector<string> unsorted_index_ba_monster_part_hex;
    for (unsigned int i = 0; i < unsorted_index_ba_monster.size(); i++)
    {
        unsorted_index_ba_monster_part_hex.push_back(getLEString(toHex(unsorted_index_ba_monster.at(i)), true, true, 2));
        // cout <<getLEString(toHex(unsorted_index_items.at(i)),true,true,2)<<endl;
    }

    /*
    cout << "Hex BA Monster unsorted" << endl;
    for (unsigned int i = 0; i < unsorted_index_ba_monster_part_hex.size(); i++)
    {
        cout << unsorted_index_ba_monster.at(i) << endl;
        cout << unsorted_index_ba_monster_part_hex.at(i) << endl;
    }
    */

    //Read item.dat binary right after injecting the translation
    //string ba_monster_translated_raw_dat = "ba_monster.dat";

    //Temp binary containing the pointers section from the previous translated binary
    string temp_ba_monster_part_dat = prepatched_binary_path;
    findAndReplace(temp_ba_monster_part_dat, "input", "temp", true);
    temp_ba_monster_part_dat.append(".part");

    //Copy the pointers section
    #ifdef _WIN32
        string commandCopy_ba_monster_part_PointersSection("/C sfk.exe partcopy ");
    commandCopy_ba_monster_part_PointersSection.append(prepatched_binary_path);
    commandCopy_ba_monster_part_PointersSection.append(" -yes -fromto ");
    commandCopy_ba_monster_part_PointersSection.append(start_offset_section);
    commandCopy_ba_monster_part_PointersSection.append(" ");
    commandCopy_ba_monster_part_PointersSection.append(end_offset_section);
    commandCopy_ba_monster_part_PointersSection.append(" ");
    commandCopy_ba_monster_part_PointersSection.append(temp_ba_monster_part_dat);

    executeCommand("cmd.exe", commandCopy_ba_monster_part_PointersSection);

    #else
        string commandCopy_ba_monster_part_PointersSection(" partcopy ");
        commandCopy_ba_monster_part_PointersSection.append(prepatched_binary_path);
        commandCopy_ba_monster_part_PointersSection.append(" -yes -fromto ");
        commandCopy_ba_monster_part_PointersSection.append(start_offset_section);
        commandCopy_ba_monster_part_PointersSection.append(" ");
        commandCopy_ba_monster_part_PointersSection.append(end_offset_section);
        commandCopy_ba_monster_part_PointersSection.append(" ");
        commandCopy_ba_monster_part_PointersSection.append(temp_ba_monster_part_dat);

        executeCommand("sfk", commandCopy_ba_monster_part_PointersSection);

    #endif

    //Convert the previous binary part to a hexdump .txt
    string temp_ba_monster_part_hex = prepatched_binary_path;
    findAndReplace(temp_ba_monster_part_hex, "input", "temp", true);
    temp_ba_monster_part_hex.append(".hex.part");
    dumpBinaryToHex(temp_ba_monster_part_dat, temp_ba_monster_part_hex);

    //Read the hexdump .txt recently created
    ifstream hex_ba_monster_part_TempRead{temp_ba_monster_part_hex};
    string temp_ba_monster_part_HexRead_Original{std::istreambuf_iterator<char>(hex_ba_monster_part_TempRead), std::istreambuf_iterator<char>()};
    hex_ba_monster_part_TempRead.close();

    //cout <<tempItemsHexRead_Original<<endl;

    //Make a copy of the section to modify it with the new alphabetical indexes
    string temp_ba_monster_part_HexRead_Alph_Patched = temp_ba_monster_part_HexRead_Original;

    //For ba_monster, iterate every 88 characters, start at offset (character) 36
    unsigned int start_offset_ba_monster = 4;
    unsigned int factor_ba_monster = 32 * 8;
    unsigned int var = 0;
    //Replace the old indexes with the new ones
    for (unsigned int i = start_offset_ba_monster; i <= temp_ba_monster_part_HexRead_Original.length(); i += factor_ba_monster)
    {
        temp_ba_monster_part_HexRead_Alph_Patched.replace(i, 4, unsorted_index_ba_monster_part_hex.at(var));
        var++;
    }

    // cout <<tempItemsHexRead_Alph_Patched<<endl;

    //Dump the whole translated binary to a txt file
    string ba_monster_translated_raw_hex = prepatched_binary_path;
    findAndReplace(ba_monster_translated_raw_hex, "input", "temp", true);
    ba_monster_translated_raw_hex.append(".hex");

    dumpBinaryToHex(prepatched_binary_path, ba_monster_translated_raw_hex);

    //Read the whole hexdump txt
    ifstream ba_monster_Read{ba_monster_translated_raw_hex};
    string string_hex_ba_monster{std::istreambuf_iterator<char>(ba_monster_Read), std::istreambuf_iterator<char>()};
    ba_monster_Read.close();

    //Replace the pointers section with the one containing the new alphabetical indexes
    findAndReplace(string_hex_ba_monster, temp_ba_monster_part_HexRead_Original, temp_ba_monster_part_HexRead_Alph_Patched, true);

    //Save the new patched hexdump to a new txt file
    string temp_injected_hex_ba_monster = prepatched_binary_path;
    findAndReplace(temp_injected_hex_ba_monster, "input", "temp", true);
    temp_injected_hex_ba_monster.append(".hex.patched");

    ofstream out_injected_hex_ba_monster;
    out_injected_hex_ba_monster.open(temp_injected_hex_ba_monster);
    writeFile(out_injected_hex_ba_monster, string_hex_ba_monster);
    out_injected_hex_ba_monster.close();

    //Recreate the item.dat binary file using the previous patched hexdump
    string injected_ba_monster_bin = prepatched_binary_path;
    findAndReplace(injected_ba_monster_bin, "input", "temp", true);

    #ifdef _WIN32
    string commandHexToBin_ba_monster("/C sfk.exe filter ");
    commandHexToBin_ba_monster.append(temp_injected_hex_ba_monster);
    commandHexToBin_ba_monster.append(" -yes  +hextobin ");
    commandHexToBin_ba_monster.append(injected_ba_monster_bin);

    executeCommand("cmd.exe", commandHexToBin_ba_monster);
    #else
    string commandHexToBin_ba_monster(" filter ");
    commandHexToBin_ba_monster.append(temp_injected_hex_ba_monster);
    commandHexToBin_ba_monster.append(" -yes  +hextobin ");
    commandHexToBin_ba_monster.append(injected_ba_monster_bin);

    executeCommand("sfk", commandHexToBin_ba_monster);
    #endif

    cout << og_ptrs_section_path<< " section patched!" << endl;
}

inline bool exists_file(const std::string &name)
{
    struct stat buffer;
    return (stat(name.c_str(), &buffer) == 0);
}

int main()
{

    // Creating dirs
    #ifdef _WIN32
    if (mkdir("input") == -1){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
        // Creating dirs
    if ( mkdir("output") == -1 ){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
        // Creating dirs
    if (mkdir("temp") == -1){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
    #else
    if (mkdir("input",0755) == -1){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
        // Creating dirs
    if ( mkdir("output",0755) == -1 ){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
        // Creating dirs
    if (mkdir("temp",0755) == -1){
        cerr << "Warning:  " <<"Dir or "<< strerror(errno)<<" already" << endl;
    }
    #endif
        cout << "All directories created."<<endl;

    string project_dir = "input"+kPathSeparator+"item.nset";

    ifstream project_stream(project_dir);
    json items_json;
    try
    {
        project_stream >> items_json;
        initGlobalVectors(items_json);
        project_stream.close();
    }
    catch (exception ex)
    {
        project_stream.close();
        cout << "Error reading item.nset file!\n Verify your 'input' dir" << endl;
        return -1;
    }

    //Exclude last to item entries, pass a list of indexes
    vector<unsigned int> exclude_items = {499, 500};

    vector<string> items;
    items = remove_non_indexable(script_entries_translation, exclude_items);

    //vector<string> sorted_items;
    //function to sort names alphabetically
    //sorted_items = alphabaticallySort(items);

    //Read ba_monster

    project_dir = "input"+kPathSeparator+"ba_monster.nset";

    project_stream.open(project_dir);
    json ba_monster_json;
    try
    {
        project_stream >> ba_monster_json;
        initGlobalVectors(ba_monster_json);
        project_stream.close();
    }
    catch (exception ex)
    {
        project_stream.close();
        cout << "Error reading ba_monster.nset file!\n Verify your 'input' dir" << endl;

        return -1;
    }

    //Exclude last to item entries, pass a list of indexes
    vector<unsigned int> exclude_ba_monster = {0, 1, 2, 4, 5, 6, 7, 8, 9, 10, 11};

    vector<string> ba_monster;
    ba_monster = remove_non_indexable(script_entries_translation, exclude_ba_monster);

    //vector<string> sorted_ba_monster;
    //function to sort names alphabetically
    //sorted_ba_monster = alphabaticallySort(ba_monster);

    vector<string> all_items_and_monsters;
    all_items_and_monsters.reserve(items.size() + ba_monster.size()); // preallocate memory
    all_items_and_monsters.insert(all_items_and_monsters.end(), items.begin(), items.end());
    all_items_and_monsters.insert(all_items_and_monsters.end(), ba_monster.begin(), ba_monster.end());

    vector<string> sorted_all_items_and_monsters;
    sorted_all_items_and_monsters = alphabaticallySort(all_items_and_monsters);

    /*
    printf("after alphabetical sorting\n");
    print(sorted_items);

    printf("after alphabetical sorting\n");
    print(sorted_ba_monster);
    */

    /*
    printf("after alphabetical sorting\n");
    print(sorted_all_items_and_monsters);
    */

    vector<int> unsorted_index_items;

    for (unsigned int i = 0; i < items.size(); i++)
    {
        bool found = false;
        for (unsigned int j = 0; j < sorted_all_items_and_monsters.size(); j++)
        {
            if (items.at(i) == sorted_all_items_and_monsters.at(j))
            {
                if (found)
                {
                    cout << "Repeated: " << sorted_all_items_and_monsters.at(j) << endl;
                }
                unsorted_index_items.push_back(j);
                found = true;
                break;
            }
        }
    }

    //Unsorted index for ba_monster
    vector<int> unsorted_index_ba_monster;

    for (unsigned int i = 0; i < ba_monster.size(); i++)
    {
        bool found = false;
        for (unsigned int j = 0; j < sorted_all_items_and_monsters.size(); j++)
        {
            if (ba_monster.at(i) == sorted_all_items_and_monsters.at(j))
            {
                if (found)
                {
                    cout << "Repeated: " << sorted_all_items_and_monsters.at(j) << endl;
                }
                unsorted_index_ba_monster.push_back(j);
                found = true;
                break;
            }
        }
    }

    vector<string> alphabet;
    vector<int> alphabet_ctrs;
    string line;
    string input_path = "input"+kPathSeparator+"alphabet.txt";

    if (!exists_file(input_path))
    {
        cout << "alphabet.txt file wasn't found.\n Verify your 'input' dir" << endl;
        return -1;
    }

    ifstream input("input"+kPathSeparator+"alphabet.txt");
    try
    {
        /* code */

        for (string line; getline(input, line);)
        {
            alphabet.push_back(line);
            alphabet_ctrs.push_back(0);
        }
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
        input.close();
        return -1;
    }

    for (unsigned int i = 0; i < alphabet.size(); i++)
    {
        /* Count items */
        // cout << alphabet.at(i) << endl;
        for (unsigned int j = 0; j < sorted_all_items_and_monsters.size(); j++)
        {
            /* code */
            string initial = sorted_all_items_and_monsters.at(j).substr(0, 1);
            if (initial == alphabet.at(i))
            {
                alphabet_ctrs.at(i)++;
            }
        }

        //cout << alphabet.at(i) << endl;
    }

    string offsets_zukan = "0000";

    unsigned int total = 0;
    for (unsigned int i = 0; i < alphabet_ctrs.size(); i++)
    {
        /* code */
        //  cout << alphabet_ctrs.at(i) << endl;
        total += alphabet_ctrs.at(i);
        //Plus +1 to incude last index
        //  cout << "Acum: " << total + 1 << endl;
        string hex_val = toHex(total);
        //  cout << "Acum Hex: " << hex_val << endl;
        offsets_zukan.append(hex_val);
    }
    offsets_zukan.append("FFFF");
    offsets_zukan = getLEString(offsets_zukan, true, true, 2);
    /*
    cout <<alphabet.size()<<endl;
    cout <<getLEString(toHex(alphabet.size()),true,true,2)<<endl;
    */
    offsets_zukan.insert(0, getLEString(toHex(alphabet.size()),true,true,2));
    /*
    cout << offsets_zukan << endl;
    cout << "Total: " << total << endl;
    */

    string zukan_raw_bin = "input"+kPathSeparator+"zuk_index_list.dat";

    if (!exists_file(zukan_raw_bin))
    {
        cout << "'zuk_index_list.dat' file wasn't found.\n Verify your 'input' dir" << endl;
        return -1;
    }

    string zukan_raw_out = "output"+kPathSeparator+"zuk_index_list.dat";

    //Make a temp copy of zukan file
    #ifdef _WIN32
    string commandCopyTempZukan("/C xcopy /F /Y ");
    commandCopyTempZukan.append(zukan_raw_bin);
    commandCopyTempZukan.append(" ");
    commandCopyTempZukan.append(zukan_raw_out);
    commandCopyTempZukan.append("*");

    executeCommand("cmd.exe", commandCopyTempZukan);
    #else
    string commandCopyTempZukan(" ");
    commandCopyTempZukan.append(zukan_raw_bin);
    commandCopyTempZukan.append(" ");
    commandCopyTempZukan.append(zukan_raw_out);
    commandCopyTempZukan.append("");

    executeCommand("cp", commandCopyTempZukan);
    #endif

    string original_hex = "2E0000001D00310039004300540079009400AF00C300D000DC00F70003010A010D012A01320136013C0150015B015E015F01640166019B01A201BC01C501DF01E901F401F701FFFFF801FA01FB01000208021702250229022D023702FFFF";

    #ifdef _WIN32
    string commandInjectZukanRanges("/C sfk.exe replace ");
    commandInjectZukanRanges.append(zukan_raw_out);
    commandInjectZukanRanges.append(" -yes -bin \"/");
    commandInjectZukanRanges.append(original_hex);
    commandInjectZukanRanges.append("/");
    commandInjectZukanRanges.append(offsets_zukan);
    commandInjectZukanRanges.append("/\"");

    executeCommand("cmd.exe", commandInjectZukanRanges);
    #else
    string commandInjectZukanRanges(" replace ");
    commandInjectZukanRanges.append(zukan_raw_out);
    commandInjectZukanRanges.append(" -yes -bin \"/");
    commandInjectZukanRanges.append(original_hex);
    commandInjectZukanRanges.append("/");
    commandInjectZukanRanges.append(offsets_zukan);
    commandInjectZukanRanges.append("/\"");

    executeCommand("sfk", commandInjectZukanRanges);
    #endif

    cout << "Patched zukan dat!" << endl;

    //Read original pointers section
    string item_1_hex = "input"+kPathSeparator+"item_1.dat";

    if (!exists_file(item_1_hex))
    {
        cout << "'item_1.dat' file wasn't found.\n Verify your 'input' dir" << endl;
        return -1;
    }

    ifstream hexItemsRead{item_1_hex};
    string stringItemsHexRead{std::istreambuf_iterator<char>(hexItemsRead), std::istreambuf_iterator<char>()};
    hexItemsRead.close();

    //Convert unsorted indexes to a vector of hex strings, little endian
    vector<string> unsorted_index_items_hex;
    for (unsigned int i = 0; i < unsorted_index_items.size(); i++)
    {
        unsorted_index_items_hex.push_back(getLEString(toHex(unsorted_index_items.at(i)), true, true, 2));
        // cout <<getLEString(toHex(unsorted_index_items.at(i)),true,true,2)<<endl;
    }

    //Read item.dat binary right after injecting the translation
    string item_translated_raw_dat = "input"+kPathSeparator+"item.dat";

    if (!exists_file(item_translated_raw_dat))
    {
        cout << "'item.dat' file wasn't found.\n Verify your 'input' dir" << endl;
        return -1;
    }

    //Temp binary containing the pointers section from the previous translated binary
    string temp_item_dat = "temp"+kPathSeparator+"item.part";
    //Make a copy the pointers section
    #ifdef _WIN32
    string commandCopyItemsPointersSection("/C sfk.exe partcopy ");
    commandCopyItemsPointersSection.append(item_translated_raw_dat);
    commandCopyItemsPointersSection.append(" -yes -fromto 0x0C 0x55D0 ");
    commandCopyItemsPointersSection.append(temp_item_dat);

    executeCommand("cmd.exe", commandCopyItemsPointersSection);
    #else
    string commandCopyItemsPointersSection(" partcopy ");
    commandCopyItemsPointersSection.append(item_translated_raw_dat);
    commandCopyItemsPointersSection.append(" -yes -fromto 0x0C 0x55D0 ");
    commandCopyItemsPointersSection.append(temp_item_dat);

    executeCommand("sfk", commandCopyItemsPointersSection);
    #endif

    //Convert the previous binary part to a hexdump .txt
    string temp_item_hex = "temp"+kPathSeparator+"item.hex.part";
    dumpBinaryToHex(temp_item_dat, temp_item_hex);

    //Read the hexdump .txt recently created
    ifstream hexItemTempRead{temp_item_hex};
    string tempItemsHexRead_Original{std::istreambuf_iterator<char>(hexItemTempRead), std::istreambuf_iterator<char>()};
    hexItemTempRead.close();

    //cout <<tempItemsHexRead_Original<<endl;

    //Make a copy of the section to modify it with the new alphabetical indexes
    string tempItemsHexRead_Alph_Patched = tempItemsHexRead_Original;

    //For items, iterate every 88 characters, start at offset (character) 36
    unsigned int start_offset_items = 4 * 9;
    unsigned int factor_items = 88;
    unsigned int var = 0;
    //Replace the old indexes with the new ones
    for (unsigned int i = start_offset_items; i <= stringItemsHexRead.length(); i += factor_items)
    {
        tempItemsHexRead_Alph_Patched.replace(i, 4, unsorted_index_items_hex.at(var));
        var++;
    }

    // cout <<tempItemsHexRead_Alph_Patched<<endl;

    //Dump the whole translated binary to a txt file
    string item_translated_raw_hex = "temp"+kPathSeparator+"item.hex";
    dumpBinaryToHex(item_translated_raw_dat, item_translated_raw_hex);

    //Read the whole hexdump txt
    ifstream hexItemRead{item_translated_raw_hex};
    string itemHexRead{std::istreambuf_iterator<char>(hexItemRead), std::istreambuf_iterator<char>()};
    hexItemRead.close();

    //Replace the pointers section with the one containing the new alphabetical indexes
    findAndReplace(itemHexRead, tempItemsHexRead_Original, tempItemsHexRead_Alph_Patched, true);

    //Save the new patched hexdump to a new txt file
    string temp_injected_hex_item = "temp"+kPathSeparator+"item.hex.patched";
    ofstream out_injected_hex;
    out_injected_hex.open(temp_injected_hex_item);
    writeFile(out_injected_hex, itemHexRead);
    out_injected_hex.close();

    //Recreate the item.dat binary file using the previous patched hexdump
    string injected_item_bin = "output"+kPathSeparator+"item.dat";
    #ifdef _WIN32
    string commandHexToBin("/C sfk.exe filter ");
    commandHexToBin.append(temp_injected_hex_item);
    commandHexToBin.append(" -yes  +hextobin ");
    commandHexToBin.append(injected_item_bin);

    executeCommand("cmd.exe", commandHexToBin);
    #else
    string commandHexToBin(" filter ");
    commandHexToBin.append(temp_injected_hex_item);
    commandHexToBin.append(" -yes  +hextobin ");
    commandHexToBin.append(injected_item_bin);

    executeCommand("sfk", commandHexToBin);
    #endif

    cout << "Items section patched!" << endl;

    vector<unsigned int> ba_2 = {6, 7, 8, 9, 10, 11, 12, 13, 14};
    vector<unsigned int> ba_3 = {0};
    vector<unsigned int> ba_4;
    for (unsigned int i = 15; i < 41; i++)
    {
        ba_4.push_back(i);
    }
    vector<unsigned int> ba_5 = {1, 2, 3, 4, 5};
    vector<unsigned int> ba_6 = {41, 42};
    vector<unsigned int> ba_7 = {15, 29, 40};
    vector<unsigned int> ba_8 = {21};
    vector<unsigned int> ba_9 = {27};

    vector<vector<unsigned int>> ba_list;
    ba_list.push_back(ba_2);
    ba_list.push_back(ba_3);
    ba_list.push_back(ba_4);
    ba_list.push_back(ba_5);
    ba_list.push_back(ba_6);
    ba_list.push_back(ba_7);
    ba_list.push_back(ba_8);
    ba_list.push_back(ba_9);

    /*
    List of part files
    */
    vector<string> ba_parts;
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_2.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_3.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_4.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_5.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_6.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_7.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_8.dat");
    ba_parts.push_back("input"+kPathSeparator+"ba_monster_9.dat");

    for (unsigned int i = 0; i < ba_parts.size(); i++)
    {
        /* code */
        if (!exists_file(ba_parts.at(i)))
        {
            string error_parts = "ba_monster_";
            error_parts.append(to_string(i + 2));
            error_parts.append(".dat' file wasn't found.\n Verify your 'input' dir");
            cout << error_parts << endl;
            return -1;
        }
    }

    /*
   List of start offsets
   */
    vector<string> ba_starts;
    ba_starts.push_back("0x20");
    ba_starts.push_back("0x4A0");
    ba_starts.push_back("0x520");
    ba_starts.push_back("0x1220");
    ba_starts.push_back("0x14A0");
    ba_starts.push_back("0x15A0");
    ba_starts.push_back("0x1720");
    ba_starts.push_back("0x17A0");

    /*
   List of end offsets
   */
    vector<string> ba_ends;
    ba_ends.push_back("0x4A0");
    ba_ends.push_back("0x520");
    ba_ends.push_back("0x1220");
    ba_ends.push_back("0x14A0");
    ba_ends.push_back("0x15A0");
    ba_ends.push_back("0x1720");
    ba_ends.push_back("0x17A0");
    ba_ends.push_back("0x1810");

    ///######## BA_MONSTER RAW

    string raw = "input"+kPathSeparator+"ba_monster.dat";

    if (!exists_file(raw))
    {
        cout << "'ba_monster.dat' file wasn't found.\n Verify your 'input' dir" << endl;
        return -1;
    }

    //Make a copy to patch it iteratively
    string raw_temp = "temp"+kPathSeparator+"ba_monster.dat";
    #ifdef _WIN32
    string commandCopyTempRaw("/C sfk.exe copy ");
    commandCopyTempRaw.append(raw);
    commandCopyTempRaw.append(" ");
    commandCopyTempRaw.append(raw_temp);
    commandCopyTempRaw.append(" -yes");

    executeCommand("cmd.exe", commandCopyTempRaw);
    #else
    string commandCopyTempRaw(" copy ");
    commandCopyTempRaw.append(raw);
    commandCopyTempRaw.append(" ");
    commandCopyTempRaw.append(raw_temp);
    commandCopyTempRaw.append(" -yes");

    executeCommand("sfk", commandCopyTempRaw);
    #endif

    /*
    string ptrs = "input"+kPathSeparator+"ba_monster_2.dat";
    string start = "0x20";
    string end = "0x4A0";
    */
    for (unsigned int i = 0; i < ba_list.size(); i++)
    {
        /* Filter indexes */
        vector<unsigned int> my_unsorted_indexes_ba_monsters;
        for (unsigned int j = 0; j < ba_list.at(i).size(); j++)
        {
            /* code */
            my_unsorted_indexes_ba_monsters.push_back(unsorted_index_ba_monster.at(ba_list.at(i).at(j)));
        }

        exec_ba_monster_files(raw_temp, ba_parts.at(i), ba_starts.at(i), ba_ends.at(i), my_unsorted_indexes_ba_monsters);
    }

    /*
    for (unsigned int i = 0; i < alphabet_ctrs.size(); i++)
    {
        cout << alphabet_ctrs.at(i) << endl;
    }
    */

    //Copy the patched ba_monster.dat to the output dir
    string raw_out = "output"+kPathSeparator+"ba_monster.dat";
    #ifdef _WIN32
    string commandCopyOutRaw("/C sfk.exe copy ");
    commandCopyOutRaw.append(raw_temp);
    commandCopyOutRaw.append(" ");
    commandCopyOutRaw.append(raw_out);
    commandCopyOutRaw.append(" -yes");

    executeCommand("cmd.exe", commandCopyOutRaw);
    #else
    string commandCopyOutRaw(" copy ");
    commandCopyOutRaw.append(raw_temp);
    commandCopyOutRaw.append(" ");
    commandCopyOutRaw.append(raw_out);
    commandCopyOutRaw.append(" -yes");

    executeCommand("sfk", commandCopyOutRaw);
    #endif

    cout << "Injection complete!" << endl;

    return 0;
}
